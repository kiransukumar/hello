# This Docker file is for building this project on Codeship Pro
# https://documentation.codeship.com/pro/languages-frameworks/nodejs/

# use Cypress provided image with all dependencies included
FROM openjdk:18
COPY target/hello-*.jar hello.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","hello.jar"]
